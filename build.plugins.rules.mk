include $(top_srcdir)/build.rules.mk

moduledir = $(pkglibdir)/plugins
# Install libraries as data; there's no need for them to be excutable
module_DATA = $(foreach file,$(filter %.dll,$(OUTPUT_FILES)),$(file) $(file).mdb) $(foreach file,$(filter %.exe,$(OUTPUT_FILES)),$(file).mdb) $(CONFIG_FILES_)
# Install executables as scripts
module_SCRIPTS = $(filter %.exe,$(OUTPUT_FILES))
